/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ep2;

/**
 *
 * @author lucasES
 */
public interface FiltrosPPM {
    
    public StructPPM RedFilter(StructPPM RGB);
    public StructPPM BlueFilter(StructPPM RGB);
    public StructPPM GreenFilter(StructPPM RGB);
    
}
