/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ep2;

import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 * @author lucasES
 */
public class ImagemMae {
    
    // atributos
    protected String numero_magico;
    protected int altura;
    protected int largura;
    protected int cor_max;

    //metodos
    public String getNumero_magico() {
        return numero_magico;
    }

    public void setNumero_magico(String numero_magico) {
        this.numero_magico = numero_magico;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getLargura() {
        return largura;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }

    public int getCor_max() {
        return cor_max;
    }

    public void setCor_max(int cor_max) {
        this.cor_max = cor_max;
    }
        public static String Ler_Arquivo(FileInputStream arquivo) {
		String linha = "";
		byte bb;
		try {
			while ((bb = (byte) arquivo.read()) != '\n') {
				linha += (char) bb;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return linha;
	}
}
