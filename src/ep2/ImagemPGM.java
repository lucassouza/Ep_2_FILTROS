/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ep2;


/**
 *
 * @author lucasES
 */
public class ImagemPGM extends ImagemMae{
    //atributos

    private int posicao;
    private String comentario_posicao;
    
    //construtores e metodos;
    
    public ImagemPGM(String numero_magico, String comentario_posicao, int altura, int largura, int cor_max){
       this.altura = altura;
       this.numero_magico = numero_magico;
       this.comentario_posicao = comentario_posicao;
       this.largura = largura;
       this.cor_max = cor_max;
    }

    public ImagemPGM() {
    }


    public int getPosicao() {
        return posicao;
    }

    public void setPosicao(String comentario) {
        
        String saida = comentario.replace("#","");
        saida = saida.replaceAll(" ", "");
        saida = saida.replaceAll("-posicaoinicialdamensagem.","");
        int saidaint = Integer.parseInt(saida);
        
        this.posicao = saidaint;
    }

    public String getComentario_posicao() {
        return comentario_posicao;
    }

    public void setComentario_posicao(String comentario_posicao) {
        this.comentario_posicao = comentario_posicao;
    }
    


    
}



