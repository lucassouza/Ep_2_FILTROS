/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ep2;

/**
 *
 * @author lucasES
 */
public class ImagemPPM extends ImagemMae implements FiltrosPPM{
    //atributos

    private char []matrix;

    //metodos

    public ImagemPPM(String numero_magico, int altura, int largura, int cor_max){
         this.numero_magico = numero_magico;
         this.altura = altura;
         this.largura = largura;
         this.cor_max = cor_max;
     }
    
    public ImagemPPM(){
        
    }
    
    public char[] getMatrix() {
        return matrix;
    }

    public void setMatrix(char[] matrix) {
        this.matrix = matrix;
    }
    
    public StructPPM RedFilter(StructPPM RGB){
        
        RGB.setRED((char)0);
        RGB.setGREEN((char)255);
        RGB.setBLUE((char)255);

        return RGB;
    }
        public StructPPM BlueFilter(StructPPM RGB){
        RGB.setRED((char)255);
        RGB.setGREEN((char)255);
        RGB.setBLUE((char)0);
        return RGB;
    }
 
        public StructPPM GreenFilter(StructPPM RGB){
        RGB.setRED((char)255);
        RGB.setGREEN((char)0);
        RGB.setBLUE((char)255);
        return RGB;
    }
}
