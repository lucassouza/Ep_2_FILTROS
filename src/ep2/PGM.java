/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ep2;

import java.io.File;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;
import java.io.FileWriter;
import javax.swing.JOptionPane;

/**
 *
 * @author lucasES
 */
public class PGM extends javax.swing.JFrame {

    /**
     * Creates new form PGM
     */
    public PGM() {
        initComponents();
        Chooser.setFileFilter(new FileNameExtensionFilter("Apenas PGM", "pgm"));
        SalvarArquivoTXT.setVisible(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PainelChooser = new javax.swing.JFrame();
        Chooser = new javax.swing.JFileChooser();
        jSeparator1 = new javax.swing.JSeparator();
        procurarPGM = new javax.swing.JButton();
        TituloEsteganografiaPGM = new javax.swing.JLabel();
        voltarPGM = new javax.swing.JButton();
        SalvarMensagem = new javax.swing.JButton();
        EnderecoPGMLabel = new javax.swing.JLabel();
        MensagemPGMLabel = new javax.swing.JLabel();
        EspacoEnderecoPGM = new java.awt.TextField();
        PainelMensagemPGM = new java.awt.ScrollPane();
        MensagemPGM = new java.awt.TextArea();
        DecodificaPGM = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        Caminhoouttxt = new javax.swing.JTextField();
        CaminhoSalvarMensagem = new javax.swing.JLabel();
        SalvarArquivoTXT = new javax.swing.JButton();
        TituloSalvarMensagem = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();

        PainelChooser.setMinimumSize(new java.awt.Dimension(725, 330));

        Chooser.setMaximumSize(new java.awt.Dimension(725, 330));
        Chooser.setMinimumSize(new java.awt.Dimension(725, 330));
        Chooser.setPreferredSize(new java.awt.Dimension(725, 330));
        Chooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChooserActionPerformed(evt);
            }
        });
        PainelChooser.getContentPane().add(Chooser, java.awt.BorderLayout.CENTER);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocation(new java.awt.Point(50, 50));
        setMaximumSize(new java.awt.Dimension(1260, 540));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 530, 750, 10));

        procurarPGM.setText("Procurar PGM");
        procurarPGM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procurarPGMActionPerformed(evt);
            }
        });
        getContentPane().add(procurarPGM, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 151, -1));

        TituloEsteganografiaPGM.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        TituloEsteganografiaPGM.setText("        Esteganografia em Imagem .pgm");
        getContentPane().add(TituloEsteganografiaPGM, new org.netbeans.lib.awtextra.AbsoluteConstraints(247, 11, 302, 34));

        voltarPGM.setText("Voltar");
        voltarPGM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                voltarPGMActionPerformed(evt);
            }
        });
        getContentPane().add(voltarPGM, new org.netbeans.lib.awtextra.AbsoluteConstraints(1170, 500, -1, -1));

        SalvarMensagem.setText("Escolher diretório destino:");
        SalvarMensagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalvarMensagemActionPerformed(evt);
            }
        });
        getContentPane().add(SalvarMensagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 220, -1, -1));

        EnderecoPGMLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        EnderecoPGMLabel.setText("Diretório da Imagem.pgm");
        getContentPane().add(EnderecoPGMLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, -1, -1));

        MensagemPGMLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        MensagemPGMLabel.setText("Mensagem encontrada:");
        getContentPane().add(MensagemPGMLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 150, 30));

        EspacoEnderecoPGM.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        EspacoEnderecoPGM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EspacoEnderecoPGMActionPerformed(evt);
            }
        });
        getContentPane().add(EspacoEnderecoPGM, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 730, 20));

        PainelMensagemPGM.add(MensagemPGM);

        getContentPane().add(PainelMensagemPGM, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 260, 720, 260));

        DecodificaPGM.setText("Decodificar");
        DecodificaPGM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DecodificaPGMActionPerformed(evt);
            }
        });
        getContentPane().add(DecodificaPGM, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, -1, -1));
        getContentPane().add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 750, 10));

        Caminhoouttxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CaminhoouttxtActionPerformed(evt);
            }
        });
        getContentPane().add(Caminhoouttxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 260, 360, -1));

        CaminhoSalvarMensagem.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        CaminhoSalvarMensagem.setText("Caminho para Arquivo.txt:");
        getContentPane().add(CaminhoSalvarMensagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 190, -1, -1));

        SalvarArquivoTXT.setText("Salvar Arquivo.txt");
        SalvarArquivoTXT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalvarArquivoTXTActionPerformed(evt);
            }
        });
        getContentPane().add(SalvarArquivoTXT, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 290, -1, -1));

        TituloSalvarMensagem.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        TituloSalvarMensagem.setText("Salvar a Mensagem Decodificada");
        getContentPane().add(TituloSalvarMensagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 20, -1, -1));

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);
        getContentPane().add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 20, 10, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    File Arquivo; //Variavel da classe PGM.
    File ArquivoOut; //Variavel da classe PGM.
    String Decodificada;
    ImagemPGM GreyImage = new ImagemPGM();
    private void voltarPGMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_voltarPGMActionPerformed
        PGM.this.dispose();
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            new JanelaInicial().setVisible(true);
            }
        });
    }//GEN-LAST:event_voltarPGMActionPerformed


    private void procurarPGMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_procurarPGMActionPerformed

        int resultado = Chooser.showOpenDialog(this);
        if ( resultado == Chooser.CANCEL_OPTION )
            return; 
        else{
            Arquivo = Chooser.getSelectedFile();
                    }
    EspacoEnderecoPGM.setText(String.valueOf(Arquivo));
    }//GEN-LAST:event_procurarPGMActionPerformed

    private void ChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChooserActionPerformed

    }//GEN-LAST:event_ChooserActionPerformed

    private void EspacoEnderecoPGMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EspacoEnderecoPGMActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EspacoEnderecoPGMActionPerformed


    
    private void DecodificaPGMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DecodificaPGMActionPerformed
     
    try {
     FileInputStream IMAGEM = new FileInputStream(EspacoEnderecoPGM.getText());
     GreyImage.setAltura(0);
     GreyImage.setLargura(0);
     GreyImage.setCor_max(0);
     GreyImage.setNumero_magico("");

    int contador = 0;
			
    String linha = ImagemPGM.Ler_Arquivo(IMAGEM);
    //pegar numero magico
    GreyImage.setNumero_magico(linha);
	if("P5".equals(GreyImage.getNumero_magico())) {
            
            //pegar altura e largura
            linha = ImagemPGM.Ler_Arquivo(IMAGEM);
            while (linha.startsWith("#")) {
                if(contador == 0){
                GreyImage.setComentario_posicao(linha);
                contador ++;
            }
                else
                linha = ImagemPGM.Ler_Arquivo(IMAGEM);
            }        
            Scanner in = new Scanner(linha); 
            if(in.hasNext() && in.hasNextInt())
                GreyImage.setAltura(in.nextInt());
            else
                JOptionPane.showMessageDialog(null,"ARQUIVO CORROMPIDO");
            if(in.hasNext() && in.hasNextInt())
                GreyImage.setLargura(in.nextInt());
            else
                JOptionPane.showMessageDialog(null,"ARQUIVO CORROMPIDO");            
            in.close();
            
            //Pegar cor max
            linha = ImagemPGM.Ler_Arquivo(IMAGEM);
            while (linha.startsWith("#")) {
            if(contador == 0){
                GreyImage.setComentario_posicao(linha);
                contador ++;
            }
                else
                linha = ImagemPGM.Ler_Arquivo(IMAGEM);
            }
            in = new Scanner(linha);
            GreyImage.setCor_max(in.nextInt());
            in.close();
            
            while (linha.startsWith("#")) {
            if(contador == 0){
                GreyImage.setComentario_posicao(linha);
                contador ++;
            }
                else
                linha = ImagemPGM.Ler_Arquivo(IMAGEM);
            }
            
            GreyImage.setPosicao(GreyImage.getComentario_posicao());
                try {
                
                    char ch = 0;
                    char saida;
                    char bite;
            
                long position = (GreyImage.getPosicao());
                IMAGEM.skip(position);
                String Mensagem = "";
                for(int u = 0; u <= ((GreyImage.getAltura())*(GreyImage.getLargura())); u++){
                
                    saida  = 0x00;
            
                    for(int i = 0; i <= 6; i++){
                       ch = (char) IMAGEM.read();
                       bite =  (char) (ch & 01);
                       saida =  (char) (saida | bite);
                       saida =  (char) (saida << 1);
                    }
                
                    ch = (char) IMAGEM.read();
                    bite = (char) (ch & 1);
                    saida = (char) (saida | bite);
			

		if (saida == '#' && u > 0){
                    break;
		}
                Mensagem += saida;
                 MensagemPGM.setText(String.valueOf(
                "Numero Mágico da Imagem: "+GreyImage.getNumero_magico()+
                "\nComentário com a posição inicial da mensagem:  " +GreyImage.getComentario_posicao()+
                "\nPosição inicial da mensagem detectada pelo programa: " +GreyImage.getPosicao() +
                "\nAltura x Largura da imagem: "+GreyImage.getAltura()+" x "+ GreyImage.getLargura()+
                "\nDimensão da Imagem: " +((GreyImage.getAltura())*(GreyImage.getLargura()))+
                "\nNível de cor máximo: " + GreyImage.getCor_max()+
                "\nA mensagem escondida é:\n--------------------------------------------------------------------\n"+
                Mensagem+"\n--------------------------------------------------------------------"));
                 Decodificada = Mensagem;
                SalvarArquivoTXT.setVisible(true);
            }

                  
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,"NÃO FOI POSSIVEL PROCESSAR O ARQUIVO");
	}
            
   
    }
    else{
        JOptionPane.showMessageDialog(null,"ERRO! ARQUIVO INVÁLIDO");
        }
        IMAGEM.close();
    }
    
    catch (IOException e) {
        JOptionPane.showMessageDialog(null,"ERRO NA ABERTURA DO ARQUIVO");
    }		
    }//GEN-LAST:event_DecodificaPGMActionPerformed

    private void SalvarMensagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalvarMensagemActionPerformed
       
        Chooser.setFileFilter(new FileNameExtensionFilter("Arquivo.txt", "txt"));
        int resultado = Chooser.showOpenDialog(this);
        
        if ( resultado == Chooser.CANCEL_OPTION )
            return; 
        else{
            ArquivoOut = Chooser.getSelectedFile();
                    }
        Caminhoouttxt.setText(String.valueOf(ArquivoOut));
        

    }//GEN-LAST:event_SalvarMensagemActionPerformed

    private void CaminhoouttxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CaminhoouttxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CaminhoouttxtActionPerformed

    private void SalvarArquivoTXTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalvarArquivoTXTActionPerformed

        try {
            FileWriter ArquivoMensagem = new FileWriter(String.valueOf(ArquivoOut));
            //PrintWriter gravarMensagem = new PrintWriter(ArquivoMensagem);
            ArquivoMensagem.write(Decodificada);
            ArquivoMensagem.close();
            JOptionPane.showMessageDialog(null,"ARQUIVO CRIADO COM SUCESSO!");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,"O ARQUIVO NÃO PÔDE SER CRIADO");
        }
        

    }//GEN-LAST:event_SalvarArquivoTXTActionPerformed

    /**
     * @param args the command line arguments
     */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel CaminhoSalvarMensagem;
    private javax.swing.JTextField Caminhoouttxt;
    private javax.swing.JFileChooser Chooser;
    private javax.swing.JButton DecodificaPGM;
    private javax.swing.JLabel EnderecoPGMLabel;
    private java.awt.TextField EspacoEnderecoPGM;
    private java.awt.TextArea MensagemPGM;
    private javax.swing.JLabel MensagemPGMLabel;
    private javax.swing.JFrame PainelChooser;
    private java.awt.ScrollPane PainelMensagemPGM;
    private javax.swing.JButton SalvarArquivoTXT;
    private javax.swing.JButton SalvarMensagem;
    private javax.swing.JLabel TituloEsteganografiaPGM;
    private javax.swing.JLabel TituloSalvarMensagem;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JButton procurarPGM;
    private javax.swing.JButton voltarPGM;
    // End of variables declaration//GEN-END:variables

}
