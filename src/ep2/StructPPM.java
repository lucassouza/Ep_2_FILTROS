/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ep2;

/**
 *
 * @author lucasES
 */
public class StructPPM {
    //atributos
    private char RED;
    private char BLUE;
    private char GREEN;

    public char getRED() {
        return RED;
    }

    public void setRED(char RED) {
        this.RED = RED;
    }

    public char getBLUE() {
        return BLUE;
    }

    public void setBLUE(char BLUE) {
        this.BLUE = BLUE;
    }

    public char getGREEN() {
        return GREEN;
    }

    public void setGREEN(char GREEN) {
        this.GREEN = GREEN;
    }

    
}
